
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'gardens_pip'
])



import pathlib
from os.path import dirname, join, normpath


this_folder = pathlib.Path (__file__).parent.resolve ()


structure = normpath (join (this_folder, "../fields/gardens/botanical"))

import sys
if (len (sys.argv) >= 2):
	glob_string = structure + '/' + sys.argv [1]
else:
	glob_string = structure + '/**/status_*.py'


print ("glob:", glob_string)

import body_scan
SCAN = body_scan.start (
	glob_string = glob_string,
	relative_path = structure,
	
	# optional
	module_paths = [	
		normpath (join (this_folder, "../fields/gardens")),
		normpath (join (this_folder, "../fields/gardens_pip"))
	],
	
	simultaneous = True,
	
	
	db_directory = normpath (join (this_folder, "db"))
)


#
#
#
